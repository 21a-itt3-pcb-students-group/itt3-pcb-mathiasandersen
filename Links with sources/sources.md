Ressources for research:
    https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php
    https://www.pcbonline.com/blog/pcb-smt-components.html
    https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types#PIN-PITCH
    https://en.wikipedia.org/wiki/Surface-mount_technology

Gather footprints for your circuit (see ressources in comments) Instructions https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6
Examine the footprints to make sure they meet the specs in the datasheet. You might need to change the footprint?
    https://youtu.be/s8mYUTix3ao


If you can’t find a footprint online you will have to make it yourself - video 8 - 14 in this playlist has instructions
    https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx


OrCAD 17.2 PCB Design Tutorial - 04 - Capture: Preparing for Manufacture
    https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4


Footprint suppliers: Ultralibrarian
    https://www.ultralibrarian.com/
Componentsearchengine
    http://componentsearchengine.com/
Octopart
    https://octopart.com/
Others: TI, Mouser, Farnell etc.
